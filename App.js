

import React, {Component} from 'react';
import {Platform, Text, View, TouchableOpacity, Linking, TouchableWithoutFeedback} from 'react-native';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';

export class App extends Component {
  constructor(props) {
    super(props);
    this.localNotify = null;
  }

  configure = () => {
    PushNotification.configure({
      requestPermissions: Platform.OS === 'ios',

      onRegister: function (token) {
        console.log('TOKEN:', token);
      },

      onNotification: function (notification) {
        console.log('NOTIFICATION:', notification);
        Linking.openURL("https://www.facebook.com/")
        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },
    });
  };
  
  buildIOSNotification = (id, title, message, data = {}, option = {}) => {
    return {
      alertAction: option.alertAction || 'view',
      category: option.category || '',
      userInfo: {
        id: id,
        item: data,
      },
    };
  };

  buildAndroidNotification = (id,title,message,data = {},option = {}) =>{
    return{
       
        alertAction: option.alertAction || "view",
        category : option.category || "",
        userInfo:{
            id:id,
            item : data,
        },
      };
  };

  showNotification = (id, title, message, data = {}, option = {}) => {
    PushNotification.localNotification({
      ...this.buildIOSNotification(id, title, message, data, option),
      title: 'Hello',
      subText: 'What',
      message: 'How are you',
      playSound: true,
      soundName: option.soundName || 'defalut',
      userInfo: false,
    });
  };

  cancelAllNotification = () => {
    if (Platform.OS === 'ios') {
      PushNotificationIOS.removeAllDeliveredNotifications();
    } else {
      PushNotification.cancelAllLocalNotifications();
    }
  };

  unregister = () => {
    PushNotification.unregister();
  };

  componentDidMount() {
    this.localNotify = new App();
    this.localNotify.configure();
  }

  render() {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          alignContent: 'center',
        }}>
        <TouchableOpacity
          onPress={() => this.showNotification()}
          style={{
            width: 200,
            height: 40,
            backgroundColor: 'red',
            marginTop: 200,
          }}>
          <Text style={{color: '#fff', marginTop: 10, marginLeft: 26}}>Send Local Notification</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default App;
